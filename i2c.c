#include "i2c.h"

static I2C_HandleTypeDef hi2c1;

void Error_Handler(void);


void i2c_init(uint32_t DevAddress) //������������� ���������� I2C � ������� ���������� HAL
{
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
	
	if (HAL_I2C_IsDeviceReady(&hi2c1, DevAddress, 2, 10) == HAL_OK)
	{
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_SET);
	}
}

int32_t I2C_MasterTransmit(uint32_t DevAddress, uint8_t* data, uint32_t Size)
{
	uint8_t data_control = 10;
	//������ ������ � ������� ���������� HAL �� ���������� I2C
	
	while(data_control != 0)	{
			if (HAL_I2C_Master_Transmit(&hi2c1, DevAddress, data, Size, HAL_MAX_DELAY) != HAL_OK)	{
				data_control--;
			}
			else {
				data_control = 0;		
			}
		}
	
	return data_control;
}

int32_t I2C_MasterReceive(uint32_t DevAddress, uint8_t* data, uint32_t Size)
{
	uint8_t data_control = 10;
	//������ ������ � ������� ���������� HAL �� ���������� I2C
	
	while(data_control != 0)	{
			if (HAL_I2C_Master_Receive (&hi2c1, DevAddress, data, Size, HAL_MAX_DELAY) != HAL_OK)	{
				data_control--;
			}
			else {
				data_control = 0;		
			}
		}
	return data_control;
}

void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

void _delay_ms(uint32_t delay) {
	HAL_Delay(delay);
}
