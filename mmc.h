#ifndef __MMC_H
#define __MMC_H

#include <stdint.h>

#define MMC5883MA_DYNAMIC_RANGE  1600000 //��������� ������ ��������� ��������� � ����������
#define MMC5883MA_RESOLUTION     65536 //�������� ����� ��� 2^16 ���
#define MMC_ADDRESS  						(0x30<<1) //����� ������� ����� �������� 0x30 = 0b00110000

#define MMC_REG_INC1_BW00		0x00			//100 Hz
#define MMC_REG_INC1_BW01		0x01			//200 Hz
#define MMC_REG_INC1_BW10		0x10			//400 Hz
#define MMC_REG_INC1_BW11		0x11			//600 Hz

uint8_t mmc_init(uint32_t DevAddress); //������� �������������
uint8_t mmc_isready(uint32_t DevAddress, uint8_t* data); //������� �������� ���������� ���������
uint8_t mmc_read_rawdata(uint32_t DevAddress, int8_t SetReset, int16_t* data); //������� ������ ����� ������
uint8_t mmc_read_calibdata(uint32_t DevAddress, int16_t* data); //������� ������ ������������� ������
uint8_t mmc_read_offsets(uint32_t DevAddress, int16_t* offsets);
uint8_t mmc_read_temp(uint32_t DevAddress, uint8_t* temp);
#endif  /* __MMC_H */
