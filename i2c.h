#ifndef __I2C_H
#define __I2C_H

#include "stm32f4xx_hal.h"

void i2c_init(uint32_t DevAddress); //������� �������������
int32_t I2C_MasterTransmit(uint32_t DevAddress, uint8_t* data, uint32_t Size); //������� ������
int32_t I2C_MasterReceive(uint32_t DevAddress, uint8_t* data, uint32_t Size); //������� ������

void _delay_ms(uint32_t delay); //������� ��������

#endif  /* __I2C_H */
