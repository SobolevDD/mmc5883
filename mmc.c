#include "mmc.h"

extern int32_t I2C_MasterTransmit(uint32_t DevAddress, uint8_t* data, uint32_t Size);
extern int32_t I2C_MasterReceive(uint32_t DevAddress, uint8_t* data, uint32_t Size);
extern void _delay_ms(uint32_t delay);

/*��������� ��� ������ ��������� �������
				�������� ���������	�����				�������� �������� - ��������*/
#define MMC_REG_XL 					0x00			//Xout Low - Xout LSB
#define MMC_REG_XH 					0x01			//Xout High - Xout MSB
#define MMC_REG_YL 					0x02			//Yout Low - Yout LSB
#define MMC_REG_YH 					0x03			//Yout High - Yout MSB
#define MMC_REG_ZL 					0x04			//Zout Low - Zout LSB
#define MMC_REG_ZH 					0x05			//Zout High - Zout MSB
#define MMC_REG_TEMP 				0x06			//Temperature - Temperature output
#define MMC_REG_STATUS 			0x07			//Status - Device status
#define MMC_REG_INCONTR0 		0x08			//Internal control 0 - Control register 0
#define MMC_REG_INCONTR1 		0x09			//Internal control 1 - Control register 1
#define MMC_REG_INCONTR2 		0x0A			//Internal control 2 - Control register 3
#define MMC_REG_XTH 				0x0B			//X Threshold - Motion detection threshold of X
#define MMC_REG_YTH 				0x0C			//Y Threshold - Motion detection threshold of Y
#define MMC_REG_ZTH 				0x0D			//Z Threshold - Motion detection threshold of Z
#define MMC_REG_ID 					0x2F			//Product ID
#define MMC_REG_INC0_SET		0x08			//Set coil
#define MMC_REG_INC0_RESET 	0x10			//Reset coil
#define MMC_MAG_MASK				0x01			//Mask for magnetometer status
#define MMC_TEMP_MASK			 	0x02			//Mask for temperature status
#define MMC_REG_INC1_SW_RST	0x80			//Reset, similar to power-up.

inline static void combine_reg_data(uint8_t reg, uint8_t data, uint8_t* msg) //������� ����������� ������ �������� � ������
{
	//��������� �������� ����� �������� � ������, ������� � ���� ������� ���� ��������
	//� ���������� �� � ���� ������ ��� �������� � ������� ���������� I2C
	msg[0] = reg;
	msg[1] = data;
}

uint8_t mmc_init(uint32_t DevAddress) //������������� �������
{
	uint8_t data2[2];
	int32_t res;

	combine_reg_data(MMC_REG_INCONTR1, MMC_REG_INC1_SW_RST, data2);
	res = I2C_MasterTransmit(DevAddress, data2, 2);//���������� ������� � ������� ��� �������� Internal control 1, ����� �������� ��� ��������
	if (res != 0) return res;
	_delay_ms(100);
	
	combine_reg_data(MMC_REG_INCONTR1, MMC_REG_INC1_BW00, data2);
	res = I2C_MasterTransmit(DevAddress, data2, 2); //���������� ������� � ������� ��� �������� Internal control 1, ����� ���������� �������� ��������� 200 ��
	if (res != 0) return res;
	_delay_ms(100);	
	
	return 0;
}

uint8_t mmc_read_temp(uint32_t DevAddress, uint8_t* temp)
{
	uint8_t data_write[2]; //������ ��� ������ � �������� �������
	uint8_t data_control;
	int32_t res;
	
	data_control = MMC_TEMP_MASK; 
	combine_reg_data(MMC_REG_INCONTR0, data_control, data_write); //����������� ������ �������� Internal control 0 � ������, ������� ���� � ���� ��������		
	res = I2C_MasterTransmit(DevAddress, data_write, 2);//���������� 0�01 � ������� Internal control 0, ����� ������ ����� ��������� �������������
	if (res != 0) return res;
	
	data_control = 0;
	while ( (data_control & MMC_TEMP_MASK) != MMC_TEMP_MASK) //�������� ���������� ���������
	{
		res = mmc_isready(DevAddress, &data_control);
		if (res != 0) return res;
	}
	
	data_control = MMC_REG_TEMP;
	res = I2C_MasterTransmit(DevAddress, &data_control, 1);//���������� ����� �������� Xout Low
	if (res != 0) return res;
	res = I2C_MasterReceive(DevAddress, temp, 1);//��������� ��������������� 7 ���� ������� � �������� Xout Low
	if (res != 0) return res;
	
	return 0;
}

uint8_t mmc_read_rawdata(uint32_t DevAddress, int8_t SetReset, int16_t* data_raw)
{
	uint8_t data_write[2]; //������ ��� ������ � �������� �������
	uint8_t data_control;
	uint8_t data_mmc[6];
	int32_t res;
	
	if (SetReset != 0x08 & SetReset != 0x10 & SetReset != 0x00)
	{return 1;}
	
	data_control = (SetReset|MMC_MAG_MASK)&0xDF; 
	combine_reg_data(MMC_REG_INCONTR0, data_control, data_write); //����������� ������ �������� Internal control 0 � ������, ������� ���� � ���� ��������		
	res = I2C_MasterTransmit(DevAddress, data_write, 2);//���������� 0�01 � ������� Internal control 0, ����� ������ ����� ��������� �������������
	if (res != 0) return res;
	
	_delay_ms(10);
	
	data_control = 0;
	while ( (data_control & MMC_MAG_MASK) != MMC_MAG_MASK) //�������� ���������� ���������
	{
		res = mmc_isready(DevAddress, &data_control);
		if (res != 0) return res;
	}
	
	data_control = MMC_REG_XL;
	res = I2C_MasterTransmit(DevAddress, &data_control, 1);//���������� ����� �������� Xout Low
	if (res != 0) return res;
	res = I2C_MasterReceive(DevAddress, data_mmc, 6);//��������� ��������������� 7 ���� ������� � �������� Xout Low
	if (res != 0) return res;
		
	//��������� ���������� ��������� � �������� ���
	data_raw[0] = ((uint16_t) data_mmc[1] << 8) | data_mmc[0];
	data_raw[1] = ((uint16_t) data_mmc[3] << 8) | data_mmc[2];
	data_raw[2] = ((uint16_t) data_mmc[5] << 8) | data_mmc[4];
	
	return 0;
}

uint8_t mmc_read_calibdata(uint32_t DevAddress, int16_t* data_calib)
{
	uint8_t data_write[2]; //������ ��� ������ � �������� �������
	int16_t data_mmc_set[3];
	int16_t data_mmc_reset[3];
	int32_t res;
	uint8_t data_control;
	
	mmc_read_rawdata(DevAddress, MMC_REG_INC0_SET, data_mmc_set);
	_delay_ms(10);
	
	mmc_read_rawdata(DevAddress, MMC_REG_INC0_RESET, data_mmc_reset);
	_delay_ms(10);
	
	data_calib[0] = (data_mmc_set[0]-data_mmc_reset[0])/2;
	data_calib[1] = (data_mmc_set[1]-data_mmc_reset[1])/2;
	data_calib[2] = (data_mmc_set[2]-data_mmc_reset[2])/2;

	data_control = 0x00;
	combine_reg_data(MMC_REG_INCONTR0, data_control, data_write);
	res = I2C_MasterTransmit(DevAddress, data_write, 1); 
	if (res != 0) return res;
	
	return 0;
}

uint8_t mmc_read_offsets(uint32_t DevAddress, int16_t* data_offset)
{
	uint8_t data_write[2]; //������ ��� ������ � �������� �������
	int16_t data_mmc_set[3];
	int16_t data_mmc_reset[3];
	int32_t res;
	uint8_t data_control;
	
	mmc_read_rawdata(DevAddress, MMC_REG_INC0_SET, data_mmc_set);
	_delay_ms(10);
	
	mmc_read_rawdata(DevAddress, MMC_REG_INC0_RESET, data_mmc_reset);
	_delay_ms(10);
	
	data_offset[0] = (data_mmc_set[0]+data_mmc_reset[0])/2;
	data_offset[1] = (data_mmc_set[1]+data_mmc_reset[1])/2;
	data_offset[2] = (data_mmc_set[2]+data_mmc_reset[2])/2;

	data_control = 0x00;
	combine_reg_data(MMC_REG_INCONTR0, data_control, data_write);
	res = I2C_MasterTransmit(DevAddress, data_write, 1); 
	if (res != 0) return res;
	
	return 0;
}

uint8_t mmc_isready(uint32_t DevAddress, uint8_t* data)
{
	int32_t res;
	uint8_t data_control = 0; //���������� ��� �������� ���������� ���������
	uint8_t var[1];
	
	data_control = MMC_REG_STATUS;
	res = I2C_MasterTransmit(DevAddress, &data_control, 1); //���������� ����� �������� Status
	if (res != 0) return res;
	
	res = I2C_MasterReceive(DevAddress, var, 1);//��������� ������� Status
	if (res != 0) return res;
	
	*data = var[0];

	return res;
}
